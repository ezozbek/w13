import React from 'react';
import './App.css'; // Make sure to import your CSS file

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  return (
    <div>
      <h1>Weekday Table</h1>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody>
          {weekdays.map((weekday, index) => (
            <tr key={index}>
              <td>{weekday}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
